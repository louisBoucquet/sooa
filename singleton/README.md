# Singleton

[home](https://gitlab.com/louisBoucquet/sooa/blob/master/README.md)

## Before

```java
public class Singleton {
  
  // fields
  
  public Singleton(*args) {
    // initialize fields
  }
  
  // methods
}
```

**problem:**

It is possible to make more instances of the class `Singleton`. This could cause problems.

For example when using a database it is best that you only open one connection (for performance reasons).

Or maybe you want some function to only be called synchronized (reading and writing to a repo).

**solution:**

Make a private constructor, this way there can't accidentaly be an instance you don't want.

We make one instance with we make accessable by a `static public` function.

## After

### classic

```java
public class Singleton {
  private static Singleton instance = new Singleton();
  
  public static Singleton getInstance() {
    return instance
  }
  
  // fields
  
  private Singleton(*args) {
    // initialize fields
  }
  
  // methods
}
```

### some syntactic sugar

This is save because the instance is final. This means that `Singleton.instance` will allways point at the same `Singleton` instance.

```java
public class Singleton {
  public static final Singleton instance = new Singleton();
  
  // fields
  
  private Singleton(*args) {
    // initialize fields
  }
  
  // methods
}
```

## Handy expansion

Imagine that we have two classes `RepoA` and `RepoB`.

We can neatly put them together in one class `Repos`.

```java
interface RepoA {
  List<A> getAll();
  A getAll(String name);
  
  void add(A a);
  void update(A a);
  void delete(A a);
}
```

```java
interface RepoB {
  List<B> getAll();
  B getAll(String name);
  
  void add(B b);
  void update(B b);
  void delete(B b);
}
```

```java
public class Repos {
  public static final RepoA repoA = new RepoAImpl();
  public static final RepoB repoB = new RepoBImpl();
}
```

Note that `RepoA` and `RepoB` have `package-private` access. This way we can put these classes in a package en the outside will only 'see' Repos and it's instances.

the classes `RepoAImpl` and `RepoAImpl` are just specific implementations of `RepoA` en `RepoB`.
